import { Module } from '@nestjs/common';
import { AirtableModule } from '../airtable/airtable.module';
import { DiscordService } from './discord.service';

@Module({
  providers: [DiscordService],
  imports: [AirtableModule]
})
export class DiscordModule {}
