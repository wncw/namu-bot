import { Injectable } from '@nestjs/common';
import * as Discord from 'discord.js';
import { AirtableService } from '../airtable/airtable.service';

@Injectable()
export class DiscordService {
    client = new Discord.Client();

    constructor(
        private airtable: AirtableService
    ) {
        this.client.login(process.env.DISCORD_BOT_KEY);

        this.client.on('ready', ()=>this.ready());
        this.client.on('message', (msg)=>this.message(msg));
    }

    async ready() {
        console.log(`Discord: ${this.client.user.tag}`);
    }

    private async message(msg) {
        if (msg.content === '/선생님') {
            msg.reply((await this.airtable.getTeachers()).map(record=>record.fields['이름']));
        } else if (msg.content.startsWith('/출석코드')) {
            this.createAttendCode(msg);
        } else if (msg.content.startsWith('/출석')) {
            this.attend(msg);
        }
    }

    private async createAttendCode(msg) {
        const member = msg.member;
        if (!member.roles.cache.find(r => r.name === '선생님')) {
            msg.reply("출석코드 생성은 선생님만 가능합니다.")
            return;
        }

        const dm = await msg.author.createDM();
        
        const channel = msg.channel.name.split('-');
        let classCode: string = channel[0]
        classCode = classCode.slice(0, 2).toUpperCase() + '-' + classCode.slice(2)
        const classNumber = channel[channel.length - 1]

        const result = await this.airtable.getStudentsFromClass(classCode, classNumber);
        if (result) {
            const pin = this.generatePin();
            await this.airtable.registerAttendCode(pin, result);
            dm.send(`출석코드는 ${pin} 입니다. 학생들에게 '/출석 ${pin}' 명령어로 출석을 요청하세요.`)
        } else {
            msg.reply(`출석코드는 수업 채널에서만 이용할 수 있습니다.`)
        }
    }

    private async attend(msg) {
        const args = msg.content.split(' ');
        if (args.length !== 2) {
            msg.reply(`'/출석 {출석코드} 형태로 입력하세요.`)
        } else {
            const classObj: any = await this.airtable.getClassByAttendCode(args[1])
            if (!classObj) {
                msg.reply(`출석코드가 잘못되었습니다. 선생님께 출석코드를 여쭈어보세요.`)
            } else {
                const student = await this.airtable.getStudentByName(msg.member.displayName);
                const log = await this.airtable.createDailyLog(student, classObj);

                msg.reply(log)
            }
        }
    }

    private generatePin () {
        const min = 0;
        const max = 9999;
        return ("0" + (Math.floor(Math.random() * (max - min + 1)) + min)).substr(-4);
    }
}
