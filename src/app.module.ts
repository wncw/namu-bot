import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DiscordModule } from './discord/discord.module';
import { AirtableModule } from './airtable/airtable.module';

@Module({
  imports: [DiscordModule, AirtableModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
