import { Injectable } from '@nestjs/common';
import * as Airtable from 'airtable-node';
import * as Memcached from 'memcached';
import * as moment from 'moment';

@Injectable()
export class AirtableService {
    airtable = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY, base: process.env.AIRTABLE_BASE });
    memcached = new Memcached();

    registerAttendCode(code, data) {
        return new Promise((resolve, reject) => {
            this.memcached.set(code, data, 3600, (err)=>{
                if (err) {
                    console.error(err);
                    reject()
                } else {
                    resolve(code);
                }
            });
        });
    }

    getClassByAttendCode(code) {
        return new Promise((resolve, reject) => {
            this.memcached.get(code, (err, data) => {
                if (err) {
                    console.error(err);
                    reject(err)
                } else {
                    resolve(data);
                }
            });
        });
    }

    async getTeachers() {
        return (await this.airtable.table('선생님')
        .list()).records
    }

    async getStudentByName(name) {
        const result = (await this.airtable.table('원생')
        .list({
            filterByFormula: `{이름} = '${name}'`,
            view: '원생관리'
        })).records;

        if (result.length > 0) {
            return result[0]
        } else {
            return null;
        }
    }

    async getStudentsFromClass(code, number) {
        const result = (await this.airtable.table('수업')
        .list({
            filterByFormula: `AND({과목코드} = '${code}', {분반} = '${number}')`,
            view: '테이블'
        })).records;

        if (result.length > 0) {
            return result[0]
        } else {
            return null;
        }
    }

    async createDailyLog(student, classObj) {
        const lastLog = (await this.airtable.table('일지')
        .list({
            filterByFormula: `AND({원생이름} = '${student.fields['이름']}', {수업명} = '${classObj.fields['수업명']}')`,
            maxRecords: 1,
            sort: [{ field: '날짜', direction: 'desc' }],
            view: '수업일지'
        })).records;

        let set = 1;
        let time = 1;
        const today = moment().format('YYYY-MM-DD');
        if (lastLog.length === 1) {
            if (lastLog[0].fields['날짜'] === today) {
                return '이미 출석했습니다.';
            }

            set = lastLog[0].fields['세트']
            time = lastLog[0].fields['회차'] + 1
            if (time > 4) {
                time = 1;
                set += 1;
            }
        }

        // Delay
        await new Promise((resolve)=>setTimeout(resolve, 1000));
        const result = this.airtable.table('일지')
        .create({ 
            records: [{
                fields: {
                    '날짜': today,
                    '원생': [ student.id ],
                    '수업': [ classObj.id ],
                    '세트': set,
                    '회차': time
                }
            }]
        })

        if (result.error) {
            console.error(result);
            return `출석에 실패했습니다. ${result.error.message}`
        } else {
            return '출석되었습니다.';
        }
    }
}
