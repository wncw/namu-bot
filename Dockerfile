FROM node:12

RUN mkdir app
WORKDIR /app


ADD ./package.json .
ADD ./yarn.lock .

RUN yarn install

ADD ./tsconfig.build.json .
ADD ./tsconfig.json .
ADD ./src .

RUN yarn run build

EXPOSE 3000

CMD [ "node", "dist/main.js" ]